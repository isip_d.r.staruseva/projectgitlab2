# Функция 1 "Калькуляятор"
def calculator():
    calcFunc = int(input("Выберете функцию: "))
    try:
        if calcFunc == 0:
            main_programm()
        elif calcFunc == 1:
            numAdd1 = int(input("Введите 1 целое число: "))
            numAdd2 = int(input("Введите 2 целое число: "))
            print(f"Результат сложения: {numAdd1 + numAdd2}")
            menu_calculator()
        elif calcFunc == 2:
            numSub1 = int(input("Введите 1 целое число: "))
            numSub2 = int(input("Введите 2 целое число: "))
            print(f"Результат вычитания: {numSub1 - numSub2}")
            menu_calculator()
        elif calcFunc == 3:
            numMult1 = int(input("Введите 1 целое число: "))
            numMult2 = int(input("Введите 2 целое число: "))
            print(f"Результат умножения: {numMult1 * numMult2}")
            menu_calculator()
        elif calcFunc == 4:
            numDiv1 = int(input("Введите 1 целое число: "))
            numDiv2 = int(input("Введите 2 целое число: "))
            print(f"Результат деления: {numDiv1 / numDiv2}")
            menu_calculator()
        elif calcFunc == 5:
            numDivInt1 = int(input("Введите 1 число: "))
            numDivInt2 = int(input("Введите 2 число: "))
            print(f"Результат деления на целое: {numDivInt1 // numDivInt2}")
            menu_calculator()
        elif calcFunc == 6:
            numRem1 = float(input("Введите 1 число: "))
            numRem2 = float(input("Введите 2 число: "))
            print(f"Результат нахождения остатка от деления: {numRem1 % numRem2}")
            menu_calculator()
        elif calcFunc == 7:
            numDeg1 = int(input("Введите 1 целое число: "))
            numDeg2 = int(input("Введите 2 целое число: "))
            print(f"Результат возведения в степень: {numDeg1 ** numDeg2}")
            menu_calculator()
        elif calcFunc == 8:
            numSqrt = int(input("Введите целое число: "))
            print(f"Результат возведения в квадрат: {math.sqrt(numSqrt)}")
            menu_calculator()
        elif calcFunc == 9:
            numFabs = int(input("Введите целое число: "))
            print(f"Вычисленный модуль числа: {math.fabs(numFabs)}")
            menu_calculator()
        elif calcFunc == 10:
            numFac = int(input("Введите целое число: "))
            print(f"Вычисленный факториал числа: {math.factorial(numFac)}")
            menu_calculator()
        elif calcFunc == 11:
            numCeil = float(input("Введите число для округления: "))
            print(f"Результат округления до ближайшего большего числа: {math.ceil(numCeil)}")
            menu_calculator()
        elif calcFunc == 12:
            a = int(input("Введите ширину: "))
            b = int(input("Введите длину: "))
            c = int(input("Введите высоту: "))
            perimetr = lambda: print(f"P={4 * a + 4 * b + 4 * c}")
            perimetr()
            menu_calculator()
        elif calcFunc == 13:
            a = int(input("Введите ширину: "))
            b = int(input("Введите длину: "))
            c = int(input("Введите высоту: "))
            square = lambda: print(f"S={2 * (a * b + a * c + b * c)}")
            square()
            menu_calculator()
    except:
        print("Вы ввели некорректные данные!")
        menu_calculator()
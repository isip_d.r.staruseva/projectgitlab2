# Функция 2 "Подсчет в строке"
def counting_in_row():
    print('''
ПОДСЧЕТ В СТРОКЕ''')
    strUser = input("Введите строку: ")
    symb = 0
    sign = 0
    space = 0
    for i in strUser:
        if i.isalnum():
            symb += 1
        elif i.isspace():
            space += 1
        else:
            sign += 1
    print(f"Количество: символов - {symb}, пробелов - {space}, запятых - {sign}.")
    menu_counting_in_row()
